<?php

/* www.ubermensch.co.kr
 * master@ubermensch.co.kr 
 */

// if config.php in config folder, set --> const ROUTE_TYPE = "mvc";
class Api extends CControl
{

    private $token;

    public function __construct()
    {
        parent::__construct(); // must put first
        $this->token = $this->load->lib("token/pageToken"); //must make page-token  in this case below  " write form submit data at client -->  write_ok accept data on server side " 
        $filter = $this->load->lib("filter/filter"); // eg : xss filtering for "content"
        if ($key == "content") {
            $this->request[$key] = $filter->exec($value);
        } else {
            $this->request[$key] = strip_tags(urldecode($value));
        }
        // load  parts in  early time
    }

    public function __destruct()
    {
        parent::__destruct(); // must put first
        // load  parts at the last
    }

    public function member_login()
    {
        //if no method-name, invoke default --> www.ubms.kr/test_mvc
        $ip = $this->ip_address();
        $data['method'] = "POST";
        $data['target'] = CConfig::SOA_API_SERVER . '/member/login';
        $data["data"] = array(
            "username" => $this->request["username"],
            "pw" => $this->request["pw"],
            "ip" => $this->request["ip"]
        );

        $result = json_decode($this->curl->exec($data));

        $_SESSION["username"] = $result->data->username;
        $_SESSION["access_token"] = $result->data->access_token;
        $_SESSION["expire"] = $result->data->expire;

        echo json_encode($result);
    }

    public function member_logout()
    {
        //if no method-name, invoke default --> www.ubms.kr/test_mvc
        $data['method'] = "DELETE";
        $data['target'] = CConfig::SOA_API_SERVER . '/member/logout';
        $data["data"] = null;
        $result = json_decode($this->curl->exec($data, $_SESSION["access_token"]));
        unset($_SESSION["username"]);
        unset($_SESSION["access_token"]);
        unset($_SESSION["expire"]);

        echo json_encode($result);
    }

    public function member_join()
    {
        $data['method'] = "POST";
        $data['target'] = CConfig::SOA_API_SERVER . '/member';
        $data["data"] = array(
            "username" => $this->request["username"],
            "phone" => $this->request["phone"],
            "farm" => $this->request["farm"],
            "pw" => $this->request["pw"]
        );
        $result = $this->curl->exec($data);

        echo $result;
    }

    public function member_deletion()
    {
        //if no method-name, invoke default --> www.ubms.kr/test_mvc
        $data['method'] = "DELETE";
        $data['target'] = CConfig::SOA_API_SERVER . '/member';
        $data["data"] = array(
            "id" => $this->request["id"]
        );
        $result = $this->curl->exec($data, $_SESSION["access_token"]);

        echo $result;
    }

    public function board_dist()
    {
        //if no method-name, invoke default --> www.ubms.kr/test_mvc
        $data['method'] = "POST";
        $data['target'] = CConfig::SOA_API_SERVER . '/board/dist';
        $data["data"] = array(
            "email" => $this->request["email"],
            "dist" => $this->request["dist"]
        );
        $result = $this->curl->exec($data, $_SESSION["access_token"]);

        echo $result;
    }

    public function board_upload()
    {
        //if no method-name, invoke default --> www.ubms.kr/test_mvc
        $data['method'] = "PUT";
        $data['target'] = CConfig::SOA_API_SERVER . '/board';
        $data["data"] = array(
            "email" => $this->request["email"],
            "total" => $this->request["total"],
            "dist" => $this->request["dist"]
        );
        $result = $this->curl->exec($data, $_SESSION["access_token"]);

        echo $result;
    }

    public function send_mail()
    {
        //if no method-name, invoke default --> www.ubms.kr/test_mvc
        $nameFrom  = "songjay";
        $mailFrom = "fcsungjae@eetech.kr";
        $nameTo  = "김숭저";
        $mailTo = "fcsungjae@naver.com";
        $cc = "";
        $bcc = "";
        $subject = "hahahah";
        $content = "sdsdasdasd";





        $charset = "UTF-8";



        $nameFrom   = "=?$charset?B?" . base64_encode($nameFrom) . "?=";
        $nameTo   = "=?$charset?B?" . base64_encode($nameTo) . "?=";
        $subject = "=?$charset?B?" . base64_encode($subject) . "?=";

        $header  = "Content-Type: text/html; charset=utf-8\r\n";
        $header .= "MIME-Version: 1.0\r\n";

        $header .= "Return-Path: <" . $mailFrom . ">\r\n";
        $header .= "From: " . $nameFrom . " <" . $mailFrom . ">\r\n";
        $header .= "Reply-To: <" . $mailFrom . ">\r\n";
        if ($cc)  $header .= "Cc: " . $cc . "\r\n";
        if ($bcc) $header .= "Bcc: " . $bcc . "\r\n";

        $result = mail($mailTo, $subject, $content, $header, $mailFrom);

        if (!$result) {
            $this->debug->log("발신실패!");
        } else {
            $this->debug->log("발신성공!");
        }
    }



    public function board_posting()
    {
        //if no method-name, invoke default --> www.ubms.kr/test_mvc
        $data['method'] = "POST";
        $data['target'] = CConfig::SOA_API_SERVER . '/board';
        $data["data"] = array(
            "email" => $this->request["email"],
            "total" => $this->request["total"],
            "dist" => $this->request["dist"],
            "remain" => $this->request["remain"]
        );
        $result = $this->curl->exec($data, $_SESSION["access_token"]);

        echo $result;
    }

    public function board_deletion()
    {
        //if no method-name, invoke default --> www.ubms.kr/test_mvc
        $data['method'] = "DELETE";
        $data['target'] = CConfig::SOA_API_SERVER . '/board';
        $data["data"] = array(
            "id" => $this->request["id"]
        );
        $result = $this->curl->exec($data, $_SESSION["access_token"]);

        echo $result;
    }

    private function ip_address()
    {
        $ipaddress = '';
        if ($_SERVER['HTTP_CLIENT_IP'])
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if ($_SERVER['HTTP_X_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if ($_SERVER['HTTP_X_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if ($_SERVER['HTTP_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if ($_SERVER['HTTP_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if ($_SERVER['REMOTE_ADDR'])
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}
