<?php

/* www.ubermensch.co.kr
 * master@ubermensch.co.kr 
 */

// if config.php in config folder, set --> const ROUTE_TYPE = "mvc";
class Board extends CControl {

    private $token;
    private $control;

    public function __construct() {
        parent::__construct(); // must put first
        $this->token = $this->load->lib("token/pageToken"); //must make page-token  in this case below  " write form submit data at client -->  write_ok accept data on server side " 
        $filter = $this->load->lib("filter/filter"); // eg : xss filtering for "content"
        if ($key == "content") {
            $this->request[$key] = $filter->exec($value);
        } else {
            $this->request[$key] = strip_tags(urldecode($value));
        }
        $this->load->view("common/top");
        $this->load->view("common/nav");
    }

    public function __destruct() {
        parent::__destruct(); // must put first
        // load  parts at the last
        $this->load->view("common/foot");
    }

    public function index() {
        $this->load->view("board/search");
    }

    public function main() {
        $page = CUri::$MAP_QUERY["page"];
        $lpp = CUri::$MAP_QUERY["lpp"];
        // $email = CUri::$MAP_QUERY["email"];
        $board = json_decode($this->board($page,$lpp));
        $view_data["board"] = $board->data;
        // $content = json_decode($this->board_content($email));
        // $view_data["content"] = $content->data;
        $this->load->view("member/board",$view_data);
    }

    public function content() {
        $email = CUri::$MAP_QUERY["email"];
        $content = json_decode($this->board_content($email));
        $view_data["content"] = $content->data;
        $this->load->view("board/content",$view_data);
    }

    public function search() {
        $this->load->view("board/search");
    }

    public function board($page, $lpp) {
        //if no method-name, invoke default --> www.ubms.kr/test_mvc
        $data['method'] = "GET";
        $data['target'] = CConfig::SOA_API_SERVER . '/board?page='.$page.'&lpp='.$lpp;
        $data["data"] = NULL;
        $result = $this->curl->exec($data);

        return $result;
    }

    public function board_content($email) {
        //if no method-name, invoke default --> www.ubms.kr/test_mvc
        $data['method'] = "GET";
        $data['target'] = CConfig::SOA_API_SERVER . '/board/content?email='.$email;
        $data["data"] = NULL;
        $result = $this->curl->exec($data);

        return $result;
    }


}
