<?php

/* www.ubermensch.co.kr
 * master@ubermensch.co.kr 
 */

// if config.php in config folder, set --> const ROUTE_TYPE = "mvc";
class Index extends CControl {

    private $token;
    private $common;

    public function __construct() {
        parent::__construct(); // must put first
        $this->token = $this->load->lib("token/pageToken"); //must make page-token  in this case below  " write form submit data at client -->  write_ok accept data on server side " 
        $filter = $this->load->lib("filter/filter"); // eg : xss filtering for "content"
        if ($key == "content") {
            $this->request[$key] = $filter->exec($value);
        } else {
            $this->request[$key] = strip_tags(urldecode($value));
        }
        // $this->common = $this->load->control("common");
        // load  parts in  early time
        // $this->common->index();
        $this->load->view("common/top");
        $this->load->view("common/nav");
    }

    public function __destruct() {
        parent::__destruct(); // must put first
        // load  parts at the last
        $this->load->view("common/foot");
    }

    public function index() {
        //if no method-name, invoke default --> www.ubms.kr/test_mvc
        $this->load->view("board/search");
    }


}
