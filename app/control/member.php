<?php

/* www.ubermensch.co.kr
 * master@ubermensch.co.kr 
 */

// if config.php in config folder, set --> const ROUTE_TYPE = "mvc";
class Member extends CControl {

    private $token;
    private $control;

    public function __construct() {
        parent::__construct(); // must put first
        $this->token = $this->load->lib("token/pageToken"); //must make page-token  in this case below  " write form submit data at client -->  write_ok accept data on server side " 
        $filter = $this->load->lib("filter/filter"); // eg : xss filtering for "content"
        if ($key == "content") {
            $this->request[$key] = $filter->exec($value);
        } else {
            $this->request[$key] = strip_tags(urldecode($value));
        }

        // load  parts in  early time
        $this->load->view("common/top");
        $this->load->view("common/nav");
    }

    public function __destruct() {
        parent::__destruct(); // must put first
        // load  parts at the last
        $this->load->view("common/foot");
    }

    public function index() {
        $page = CUri::$MAP_QUERY["page"];
        $lpp = CUri::$MAP_QUERY["lpp"];
        $board = json_decode($this->board($page,$lpp));
        $view_data["board"] = $board->data;
        $this->load->view("member/main",$view_data);
    }

    public function main() {
        $page = CUri::$MAP_QUERY["page"];
        $lpp = CUri::$MAP_QUERY["lpp"];
        $board = json_decode($this->board($page,$lpp));
        $view_data["board"] = $board->data;
        $this->load->view("member/main",$view_data);
    }

    public function login() {
        $this->load->view("member/login");
    }

    public function join() {
        $this->load->view("member/join");
    }

    public function board($page, $lpp) {
        //if no method-name, invoke default --> www.ubms.kr/test_mvc
        $data['method'] = "GET";
        $data['target'] = CConfig::SOA_API_SERVER . '/member?page='.$page.'&lpp='.$lpp;
        $data["data"] = NULL;
        $result = $this->curl->exec($data);

        return $result;
    }

}
