<?php

/* www.ubermensch.co.kr
 * master@ubermensch.co.kr 
 */

// if config.php in config folder, set --> const ROUTE_TYPE = "mvc";
class Swap extends CControl {

    private $token;
    private $common;

    public function __construct() {
        parent::__construct(); // must put first
        // $this->common->index();
        $this->load->view("common/top");
    }

    public function __destruct() {
        parent::__destruct(); // must put first
        // load  parts at the last
    }

    public function info() {
        //if no method-name, invoke default --> www.ubms.kr/test_mvc
        $email = CUri::$MAP_QUERY["email"];
        $content = json_decode($this->board_content($email));
        $view_data["content"] = $content->data;
        $this->load->view("swap/info",$view_data);
    }

    public function board_content($email) {
        //if no method-name, invoke default --> www.ubms.kr/test_mvc
        $data['method'] = "GET";
        $data['target'] = CConfig::SOA_API_SERVER . '/board/content?email='.$email;
        $data["data"] = NULL;
        $result = $this->curl->exec($data);

        return $result;
    }


}
