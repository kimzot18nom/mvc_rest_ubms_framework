<main class="main_container">
    <div class="innerbox">
        <div class="user_detail_wrap w_height">
            <h3>유저정보</h3>
            <p><span>이메일</span><?=$content->email?></p>
            <p><span>농장 아이디</span><?=$content->farm?></p>
            <p><span>전화번호</span><?=$content->phone?></p>
            <p><span>총 수량</span><?=$content->total?></p>
            <p><span>분배된 수량</span><?=$content->dist?></p>
            <p><span>남은 수량</span><?=$content->remain?></p>
        </div>
    </div>
</main>