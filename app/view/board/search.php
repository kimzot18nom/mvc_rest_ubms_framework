<main class="main_container">
        <div class="innerbox">
            <div class="email_search_wrap w_height">
                <h3>이메일 검색</h3>
                <input id="EMAIL" type="text" placeholder="찾으실 이메일을 입력해 주세요.">
                <button id="SEARCH" type="submit" class="pointer">이메일 검색</button>
            </div>
        </div>
        <div class="search_result_wrap">
            <h4>검색결과</h4>
            <table class="list_table">
                <caption>유저 리스트</caption>
                <colgroup class="user_list">
                    <col class="col1" style="width:10%">
                    <col class="col2" style="width:20%">
                    <col class="col3" style="width:10%">
                    <col class="col4" style="width:15%">
                    <col class="col5" style="width:15%">
                    <col class="col6" style="width:10%">
                    <col class="col7" style="width:20%">
                </colgroup>
                <thead>
                    <tr>
                        <th>번호</th>
                        <th>이메일</th>
                        <th>총량</th>
                        <th>분배된량</th>
                        <th>남은수량</th>
                        <th>등록일</th>
                        <th>마지막 분배일</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>2</td>
                        <td>2</td>색
                        <td>4</td>
                    </tr>
                </tbody>
            </table>
            <button type="button" class="back_btn_2">닫기</button>
        </div>
    </main>

    <script>
        $(document).ready(function(){

            $("#SEARCH").on("click",function(){
                var email = $("#EMAIL").val();
                board.go("/board/content?email="+email);
            });
        });
    </script>