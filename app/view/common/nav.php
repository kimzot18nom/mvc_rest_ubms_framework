<body>
    <header class="main_header">
        <div class="innerbox">
            <button type="button" class="nav_btn"></button>
            <nav class="gnb">
                <ul>
                    <li><a href="/board/search">이메일검색</a></li>
                    <li><a href="/board/main">회원분배리스트</a></li>
                    <!-- <li><a href="/board/content">유저정보</a></li> -->
                    <li><a href="/member">관리자 리스트</a></li> 
                    <?php
                    if(!$_SESSION["access_token"]){
                    ?>
                    <li><a href="/member/login">로그인</a></li>
                    <li><a href="/member/join">회원가입</a></li>
                    <?php
                    }
                    ?>
                </ul>
            </nav>
        </div>
    </header>