<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1, minimum-scale=1, width=device-width">
        <title>anifarm</title>
        <link rel="stylesheet" href="/css/reset.css">
        <link rel="stylesheet" href="/css/ubms.css">
        <link rel="stylesheet" href="/css/style.css">
        <script src="/js/jquery-3.5.1.js"></script>
        <script src="/js/js.js"></script>
        <script src="/js/ubms.js"></script>
    </head>