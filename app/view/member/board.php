<main class="main_container">
        <div class="innerbox">
            <div class="user_list_wrap w_height">
                <h3>회원분배리스트</h3>
                <div class="list_wrap">
                    <!-- <button type="button" class="add_btn pointer">추가</button> -->
                    <table class="list_table">
                        <caption>유저 리스트</caption>
                        <colgroup class="admin_list">
                            <col class="col1">
                            <col class="col2">
                            <col class="col3">
                            <col class="col4">
                            <col class="col7">
                            <col class="col6">
                            <col class="col7">
                            <col class="col7">
                            <col class="col9">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>번호</th>
                                <th>이메일</th>
                                <th>전화번호</th>
                                <th>농장아이디</th>
                                <th>총량</th>
                                <th>분배된량</th>
                                <th>남은량</th>
                                <th>지급</th>
                                <th>삭제</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach($board->list as $item){
                            ?>
                            <tr>
                            <td><a status="<?=$item->id?>" class="pointer"><?=$item->id?></a></td>
                                <td><a class="pointer" status="<?=$item->id?>"><?=$item->email?></a></td>
                                <td><a class="pointer" status="<?=$item->id?>"><?=$item->phone?></a></td>
                                <td><a class="pointer" status="<?=$item->id?>"><?=$item->farm?></a></td>
                                <td>
                                    <div class="right_box">
                                        <input class="total" value="<?=$item->total?>" type="number">
                                        <button type="button" status="<?=$item->email?>" class="pointer update">지급</button>
                                    </div>
                                </td>
                                <td><a class="pointer" status="<?=$item->id?>"><?=$item->dist?></a></td>
                                <td><a class="pointer" status="<?=$item->id?>"><?=$item->remain?></a></td>
                                <td>
                                    <div class="right_box">
                                        <input class="amount" type="number">
                                        <button type="button" status="<?=$item->email?>" class="pointer dist">지급</button>
                                    </div>
                                </td>
                                <td>
                                    <div class="delete_box">
                                        <button status="<?=$item->id?>" type="button" class="pointer delete_btn">삭제</button>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                    <ul class="list_num">
                        <?php
                        $page = CUri::$MAP_QUERY["page"];

                        if(empty($page)){
                            $page = 1;
                        }
                        
                        if($page > 1){
                        ?>
                        <li><a href="/board/main?page=<?=$page - 1?>">&#60;</a></li>
                        <?php
                        }else{

                        ?>
                        <li><a>&#60;</a></li>
                        <?php
                        }

                        for($i=1;$i<= $board->page_count+1; $i+1){
                        ?>
                        <li><a href="/board/main?page=<?=$i?>"class="pointer"><?=$i?></a></li>
                        <?php
                        $i++;
                        }

                        if($page < $board->page_count){
                        ?>
                        <li><a href="/board/main?page=<?=$page + 1?>"class="pointer">&#62;</a></li>
                        <?php
                        }else{

                        ?>
                        <li><a>&#62;</a></li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="add_wrap" id="INFO">
            </div>
            <!-- <div class="list_fix user_detail_wrap w_height">
                <h3>유저정보</h3>
                <p><span>아이디</span>유저 정보입니다</p>
                <p><span>이메일</span>유저 정보입니다</p>
                <p><span>총량</span>유저 등록일입니다.</p>
                <p><span>분배된량</span>유저 등록일입니다.</p>
                <p><span>남은수량</span>유저 정보입니다</p>
                <button type="button" class="pointer back_btn_2">취소</button>
            </div> -->
        </div>
    </main>

    <script>
    $(document).ready(function() {
        $(".delete_btn").on("click", function() {
            var id = $(this).attr("status");

            if (!id) {
                alert("대상이 지정되지 않았습니다.");
                return;
            }


            if (!confirm(id + "번 회원을 삭제하시겠습니까?")) {
                alert("취소");
                return;
            }

            var data = {
                "id": id
            }

            var res = board.request("/api/board_deletion", data);

            if (!res.data.email) {
                alert(res.data.message);
                return;
            } else {
                alert("회원을 삭제 하셨습니다.");
                board.re();
            }
        });

        $(".dist").on("click", function() {
            var email = $(this).attr("status");
            var dist = $(this).prev().val();

            if (!email) {
                alert("이메일을 입력해 주세요.");
                return;
            }

            if (!dist) {
                alert("분배량을 입력해 주세요.");
                return;
            }

            if (!confirm(dist +"개를 분배 하시겠습니까?")) {
                alert("취소");
                return;
            }


            var data = {
                "email": email,
                "dist": dist
            }

            var res = board.request("/api/board_dist", data);

            if (!res.data.email) {
                alert(res.data.message);
                return;
            } else {
                alert("분배에 성공하셨습니다.");
                board.re();
            }
        });

        $(".update").on("click", function() {
            var email = $(this).attr("status");
            var total = $(this).prev().val();

            if (!email) {
                alert("이메일을 입력해 주세요.");
                return;
            }

            if (!total) {
                alert("총량을 입력해 주세요.");
                return;
            }

            if (!confirm(total +"개를 분배 하시겠습니까?")) {
                alert("취소");
                return;
            }


            var data = {
                "email": email,
                "total": total
            }

            var res = board.request("/api/board_upload", data);

            if (!res.data.email) {
                alert(res.data.message);
                return;
            } else {
                alert("분배에 성공하셨습니다.");
                board.re();
            }
        });
    });
</script>