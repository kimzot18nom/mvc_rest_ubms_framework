<main class="main_container">
    <div class="innerbox">
        <div class="login_wrap w_height">
            <h3>회원가입</h3>
            <p>이메일</p>
            <input id="EMAIL" type="text" placeholder="이메일 입력하세요.">
            <p>전화번호</p>
            <input id="PHONE" type="text" placeholder="전화번호를 입력하세요.">
            <p>농장아이디</p>
            <input id="FARM" type="text" placeholder="농장아이디를 입력하세요.">
            <p>비밀번호</p>
            <input id="PW" type="password" placeholder="비밀번호를 입력하세요.">
            <p>비밀번호 확인</p>
            <input id="PW2" type="password" placeholder="비밀번호를 다시 입력하세요.">
            <button id="SUBMIT" type="submit" class="pointer">회원가입</button>
        </div>
    </div>
</main>


<script>
    $(document).ready(function() {
        $("#SUBMIT").on("click", function() {
            var email = $("#EMAIL").val();
            var phone = $("#PHONE").val();
            var farm = $("#FARM").val();
            var pw = $("#PW").val();
            var pw2 = $("#PW2").val();

            if (!email) {
                alert("이메일을 입력해 주세요.");
                return;
            }

            if (!phone) {
                alert("전화번호를 입력해 주세요.");
                return;
            }

            if (!farm) {
                alert("농장아이디를 입력해 주세요.");
                return;
            }

            if (!pw) {
                alert("비밀번호를 입력해 주세요.");
                return;
            }

            if (pw !== pw2) {
                alert("두개의 비밀번호가 일치하지 않습니다.");
                return;
            }

            var data = {
                "username": email,
                "phone": phone,
                "farm": farm,
                "pw": pw
            }

            var res = board.request("/api/member_join", data);

            if (!res.data.email) {
                alert(res.data.message);
                return;
            } else {
                alert("회원가입에 성공하셨습니다.");
                return;
            }
        });
    });
</script>