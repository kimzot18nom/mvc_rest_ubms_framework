<main class="main_container">
    <div class="innerbox">
        <div class="login_wrap w_height">
            <h3>로그인</h3>
            <p>이메일</p>
            <input id="EMAIL" type="text" placeholder="이메일을 입력하세요.">
            <p>비밀번호</p>
            <input id="PW" type="password" placeholder="비밀번호를 입력하세요.">
            <button id="SUBMIT" type="submit" class="pointer">로그인</button>
        </div>
    </div>
</main>


<script>
    $(document).ready(function() {
        $("#SUBMIT").on("click", function() {
            var email = $("#EMAIL").val();
            var pw = $("#PW").val();

            if (!email) {
                alert("이메일을 입력해 주세요.");
                return;
            }

            if (!pw) {
                alert("비밀번호를 입력해 주세요.");
                return;
            }


            var data = {
                "username": email,
                "pw": pw
            }

            var res = board.request("/api/member_login", data);

            if (!res.data.username) {
                alert(res.data.message);
                return;
            } else {
                alert("회원가입에 성공하셨습니다.");
                board.go("/");
            }
        });
    });
</script>