<main class="main_container">
        <div class="innerbox">
            <div class="user_list_wrap w_height">
                <h3>관리자 리스트</h3>
                <div class="list_wrap">
                    <button type="button" class="add_btn pointer">추가</button>
                    <table class="list_table">
                        <caption>유저 리스트</caption>
                        <colgroup class="user_list">
                            <col class="col1" style="width:12%">
                            <col class="col2" style="width:15%">
                            <col class="col7" style="width:15%">
                            <col class="col8" style="width:10%">
                            <!-- <col class="col1">
                            <col class="col2">
                            <col class="col3">
                            <col class="col4">
                            <col class="col5">
                            <col class="col6">
                            <col class="col7">
                            <col class="col8">
                            <col class="col9"> -->
                        </colgroup>
                        <thead>
                            <tr>
                                <th>번호</th>
                                <th>이메일</th>
                                <th>등록일</th>
                                <!-- <th>지급</th>-->
                                <th>삭제</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach($board->list as $item){
                            ?>
                            <tr>
                                <td><a class="pointer"><?=$item->id?></a></td>
                                <td><a class="pointer"><?=$item->username?></a></td>
                                <td><a class="pointer"><?=$item->reg_date?></a></td>
                                <!-- <td>
                                    <div class="right_box">
                                        <input type="number">
                                        <button type="button" class="pointer">지급</button>
                                    </div>
                                </td>-->
                                <td>
                                    <div class="delete_box">
                                        <button type="button" class="pointer delete_btn">삭제</button>
                                    </div>
                                </td> 
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="add_wrap pop_up">
                <div class="admin_add w_height">
                    <h3>어드민 등록</h3>
                    <ul>
                        <li>
                            <p>아이디</p><input type="text" placeholder="이메일">
                        </li>
                        <li>
                            <p>비밀번호</p><input type="text" placeholder="비밀번호">
                        </li>
                    </ul>
                    <div class="btn_wrap">
                        <button type="button" class="pointer admin_add_btn">동록하기</button>
                        <button type="button" class="pointer admin_add_btn back_btn">취소</button>
                    </div>
                </div>
            </div>
            <div class="list_fix user_detail_wrap w_height pop_up">
                <h3>유저정보</h3>
                <p><span>아이디</span>유저 정보입니다</p>
                <p><span>이메일</span>유저 정보입니다</p>
                <p><span>총량</span>유저 등록일입니다.</p>
                <p><span>분배된량</span>유저 등록일입니다.</p>
                <p><span>남은수량</span>유저 정보입니다</p>
                <button type="button" class="pointer back_btn_2">취소</button>
            </div>
        </div>
    </main>