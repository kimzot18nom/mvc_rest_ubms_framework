<h4>회원정보</h4>
<ul>
    <li>
        <p>이메일</p><input type="text" value="<?=$content->email?>" placeholder="이메일" disabled>
    </li>
    <li>
        <p>총량</p><input type="text" value="<?=$content->total?>" placeholder="총량" disabled>
    </li>
    <li>
        <p>분배 된량</p><input type="text" value="<?=$content->dist?>" placeholder="분배된량" disabled>
    </li>
    <li>
        <p>남은 수량</p><input type="text" value="<?=$content->remain?>" placeholder="남은수량" disabled>
    </li>
</ul>
<div class="btn_wrap">
    <button type="button" class="pointer add_wrap_btn">수정하기</button>
    <button type="button" class="pointer add_wrap_btn back_btn">취소</button>
</div>